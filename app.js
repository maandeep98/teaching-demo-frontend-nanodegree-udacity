$(function() {

    //input management
    let height = $('#gheight');
    let width = $('#gwidth');
    let colour = $('#gcolor');

    const table = $('#pixelart');

    function makeart() {
        table.find("tbody").remove();

        let row,col;
        row = height.val();
        col = width.val();

        table.append('<tbody></tbody>');
        let tb = table.find('tbody');

        for(let i = 0; i < row; i++) {
            tb.append('<tr></tr>');
        }

        for(let i=0; i < col; i++) {
            table.find('tr').append('<td class="white"></td>');
        }

    }

    $(document).ready(function () {
        //submit button function
       $('#sub').on('click', function (e) {
           e.preventDefault();
         makeart();
       });

       //change colour
        $('#pixelart').on('click','td', function() {
           let c = colour.val();
           let currentcolour =  $('this').css('background-color');

           if($(this).hasClass('white')) {
               $(this).toggleClass('white');
               $(this).css('background-color', c);
           } else {
               $(this).toggleClass('white');
               $(this).css('background-color', "white");
           }

        });

    });

});